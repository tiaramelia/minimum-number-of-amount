FROM node:latest

#Create app directory
RUN mkdir -p /usr/src/amount-app

WORKDIR /usr/src/amount-app

COPY package.json /usr/src/amount-app/

RUN npm install

ADD src /usr/src/amount-app/src
ADD public /usr/src/amount-app/public

RUN npm run build

CMD [ "npm", "start" ]

import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Amount from './pages/amount'

export default <Router>
    <div>
        <Switch>
            <Route exact path="/" component={Amount} />
            <Route exact path="/amount" component={Amount} />
        </Switch>
    </div>
</Router>
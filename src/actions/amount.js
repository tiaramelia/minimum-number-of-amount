import Types from './types'

export function addRow (key) {
    let data = [];
    for (let i = 1; i <= parseInt(key); i++) {
        data.push(Object.assign({}, {
            action: "ADD",
            amount: "",
            seratus_ribu: 0,
            limapuluh_ribu: 0,
            duapuluh_ribu: 0,
            sepuluh_ribu: 0,
            lima_ribu: 0,
            seribu: 0,
            lima_ratus: 0,
            seratus: 0,
            limapuluh: 0,
            sisa: 0,
        }));
    }

    return {
        type: Types.TYPE_REQUEST_AMOUNT.ADD_ROW,
        payload: data
    };
}

export function SetDetailRow(value) {
    return function(dispatch) {
        dispatch({
            type: Types.TYPE_REQUEST_AMOUNT.SET_DETAIL_ROW,
            payload: value
        })
    }
}

export function DeleteRow(value) {
    return function(dispatch) {
        dispatch({
            type: Types.TYPE_REQUEST_AMOUNT.DELETE_ROW,
            payload: value
        })
    }
}

export function ChangeValue (key, value, index) {
    return {
        type: Types.TYPE_REQUEST_AMOUNT.CHANGE_VALUE,
        payload: {key: key, value: value, index: index}
    };
}

export function SetErrorMessage (error) {
    return {
        type: Types.TYPE_REQUEST_AMOUNT.REJECTED_AMOUNT,
        payload: {errors: error}
    };
}
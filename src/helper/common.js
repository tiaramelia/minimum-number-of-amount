import imgtitle from '../assets/amount.svg';

class Common {
    constructor () {
        this.state = {
            rowData: {},
        }
    }
    setTitle (titleName = "Harmony", linkimg = imgtitle) {
        let favicon = document.createElement("link")
        favicon.rel = "icon"
        favicon.href = linkimg
        document.head.appendChild(favicon)
        document.title = titleName;
    }

}
let Common2 = new Common()

export default Common2;
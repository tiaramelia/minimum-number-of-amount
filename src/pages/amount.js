import React, { Component } from 'react';
import { connect } from 'react-redux';
import {addRow, SetDetailRow, DeleteRow, ChangeValue, SetErrorMessage} from "../actions/amount"
import deleteicon from "../assets/delete_ico_24.svg"
import 'antd/dist/antd.css';
import '../style.scss'
import {Convert} from "../helper"
import DetailTable from "./detailtable"
import Common from "../helper/common"
import {Alert} from 'antd'

class Amount extends Component {
    constructor(props){
        super(props);

        this.state = {
            data : {
                amount: "TOTAL",
                seratus_ribu: 0,
                limapuluh_ribu: 0,
                duapuluh_ribu: 0,
                sepuluh_ribu: 0,
                lima_ribu: 0,
                seribu: 0,
                lima_ratus: 0,
                seratus: 0,
                limapuluh: 0,
                sisa: 0,
                total:true
            }
        }
        this.onBlurlistAmount = this.onBlurlistAmount.bind(this)
        this.onKeyPress = this.onKeyPress.bind(this)
    }

    onChangeDetail (key, value, index) {
        this.props.dispatch(ChangeValue(key, value, index))
    }

    onKeyPress (event) {
        if (event.charCode == 13){
            this.onBlurlistAmount(event.target.id)
        }
    }

    validate (amount, index) {
        if (/\d/.test(amount)){
            if (amount.startsWith("rp") || amount.startsWith("Rp" || amount.startsWith("RP"))) {
                return true
            } else if (amount.endsWith("rp") || amount.endsWith("Rp" || amount.endsWith("RP"))) {
                this.props.dispatch(SetErrorMessage({message: "Valid character in wrong position!", path: "amount", index: index}))
                return false
            } else {
                if (amount.split(",").length == 2 && amount.split(",")[1] == "00"){
                    return true
                } else if (isNaN(amount)){
                    this.props.dispatch(SetErrorMessage({message: "Invalid Input or Invalid Separator!", path: "amount", index: index}))
                    return false
                } else {
                    return true
                }
            }
        } else {
            this.props.dispatch(SetErrorMessage({message: "Missing Value!", path: "amount", index: index}))
            return false
        }
        
    }
    
    onBlurlistAmount (index) {
        let amount = this.props.amount.model.data[index].amount.replace(/[^\d\,\-]/g, "")
        if (this.validate(amount, index)){
            if (amount.split(",").length == 2 && amount.split(",")[1] == "00"){
                amount = amount.split(",")[0]
            }

            let seratus_ribu = Math.floor(amount / 100000)
            amount = amount - (seratus_ribu * 100000)
            let limapuluh_ribu = Math.floor(amount / 50000)
            amount = amount - (limapuluh_ribu * 50000)
            let duapuluh_ribu = Math.floor(amount / 20000)
            amount = amount - (duapuluh_ribu * 20000)
            let sepuluh_ribu = Math.floor(amount / 10000)
            amount = amount - (sepuluh_ribu * 10000)
            let lima_ribu = Math.floor(amount / 5000)
            amount = amount - (lima_ribu * 5000)
            let seribu = Math.floor(amount / 1000)
            amount = amount - (seribu * 1000)
            let lima_ratus = Math.floor(amount / 500)
            amount = amount - (lima_ratus * 500)
            let seratus = Math.floor(amount / 100)
            amount = amount - (seratus * 100)
            let limapuluh = Math.floor(amount / 50)
            amount = amount - (limapuluh * 50)
            let sisa = amount

            this.onChangeDetail("seratus_ribu", seratus_ribu, index)
            this.onChangeDetail("limapuluh_ribu", limapuluh_ribu, index)
            this.onChangeDetail("duapuluh_ribu", duapuluh_ribu, index)
            this.onChangeDetail("sepuluh_ribu", sepuluh_ribu, index)
            this.onChangeDetail("lima_ribu", lima_ribu, index)
            this.onChangeDetail("seribu", seribu, index)
            this.onChangeDetail("lima_ratus", lima_ratus, index)
            this.onChangeDetail("seratus", seratus, index)
            this.onChangeDetail("limapuluh", limapuluh, index)
            this.onChangeDetail("sisa", sisa, index)
        }
        this.calculate()
    }

    calculate () {
        let data = this.props.amount.model.data.reduce((total, row) => {
            total.seratus_ribu += row.seratus_ribu
            total.limapuluh_ribu += row.limapuluh_ribu
            total.duapuluh_ribu += row.duapuluh_ribu
            total.sepuluh_ribu += row.sepuluh_ribu
            total.lima_ribu += row.lima_ribu
            total.seribu += row.seribu
            total.lima_ratus += row.lima_ratus
            total.seratus += row.seratus
            total.limapuluh += row.limapuluh
            total.sisa += row.sisa

            return total
        }, {
            amount: "TOTAL",
            seratus_ribu: 0,
            limapuluh_ribu: 0,
            duapuluh_ribu: 0,
            sepuluh_ribu: 0,
            lima_ribu: 0,
            seribu: 0,
            lima_ratus: 0,
            seratus: 0,
            limapuluh: 0,
            sisa: 0,
            total: true
        })
        this.setState({data})
    }
    deleteRow (index) {
        if (index == 0 && this.props.amount.model.data.length == 1) {
            this.props.dispatch(SetDetailRow(index))
        } else {
            this.props.dispatch(DeleteRow(index))
        }
        this.calculate()
    }
    addRow (key) {
        this.props.dispatch(addRow(key))
    }
    renderDetailTable () {
        return <DetailTable
            addRow={(key) => this.addRow(key)}
            data={this.props.amount.model.data}
            total={this.state.data}
            dataAmount={this.props.amount.data_amount}
            error= {this.props.amount.error}
            onKeyPress={this.onKeyPress}
            onChange={(key, value, index) => this.onChangeDetail(key, value, index)}
            deleteRow={(index) => this.deleteRow(index)}
            onBlurListItem={this.onBlurlistAmount} />
    }

    renderError () {
        return <div id="col-warning" className="p--25">
        <Alert showIcon
            message={[<ul key="message-eror" className={"font-weight-normal "}>{this.props.amount.error.errors.message} </ul>]}
            type={"error"}
        />
    </div>
    }

    render() {
        Common.setTitle("Amount Apps")

        return (
            <div className="div-body">
                <div className="div-reference">
                    <h2>Calculate the minimum number of rupiahs</h2>
                    <label>Type amount in input below, and you can clik enter or tab to calculate your amount into the minimum number of rupiahs needed to make that amount.</label><br/>
                    <label>You can click "add more row" to add more amount.</label>
                </div>
                {this.props.amount.error ? this.renderError() : null}
                {this.renderDetailTable()}
            </div>
        )
    }
}

export default connect((store) => store)(Amount);
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Convert} from '../helper/converter';
import deleteicon from "../assets/delete_ico_24.svg"
import {Menu, Dropdown} from 'antd'
import {DynamicTable} from "../layout/table";

class DetailTable extends Component {
    constructor (props) {
        super(props);

        this.state = {
        }
    }
    render () {
        const TableHeader = [
            {
                label: "Amount",
                isrequired:true,
                className: 'text-left',
                classLabel: "font-size--12 pl--25"
            },
            {
                label: "100.000",
                isrequired:false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "50.000",
                isrequired:false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "20.000",
                isrequired:false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "10.000",
                isrequired:false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "5.000",
                isrequired:false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "1.000",
                isrequired: false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "500",
                isrequired: false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "100",
                isrequired: false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "50",
                isrequired: false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: "sisa (Rp.)",
                isrequired: false,
                className: 'text-right col-width-10',
                classLabel: "font-size--12"
            },
            {
                label: '',
                isrequired: false,
                className: ' wd--80 pr--25'
            }
        ];
        const header = TableHeader.map((x, index) => {
            if (x.isrequired) {
                return {content: <label key={index} className={x.classLabel}>{x.label}<span className={this.props.disabled ? "hidden" : "required"}> *</span></label>, className: x.className};
            }
            else {
                return {content: <label key={index} className={x.classLabel}>{x.label}</label>, className: x.className};
            }
        });

        let data = this.props.data.concat(this.props.total)
        let indexError = this.props.error ? this.props.error.errors.index : ""
        let TableRow = data.map((x, index) => {
            return [{
                content: x.total ? <label className="label-currname">{x.amount}</label> : <div>
                    <input
                        value={x.amount}
                        id={index}
                        placeholder="Type to convert amount"
                        disabled={this.props.disabled} minLength={2}
                        maxLength={100}
                        onKeyPress={this.props.onKeyPress}
                        className={this.props.error && indexError == index ? "input-error input-type" : "input-type"}
                        onBlur={() => this.props.onBlurListItem(index)}
                        data={this.props.dataAmount[index]} 
                        onChange={(e) => { this.props.onChange("amount", e.target.value, index); }} />

                </div>, className: "text-left pl--25 mw--200"
            },
            {content: <label>{x.seratus_ribu}</label>, className: "text-right"},
            {content: <label>{x.limapuluh_ribu}</label>, className: "text-right"},
            {content: <label>{x.duapuluh_ribu}</label>, className: "text-right"},
            {content: <label>{x.sepuluh_ribu}</label>, className: "text-right"},
            {content: <label>{x.lima_ribu}</label>, className: "text-right"},
            {content: <label>{x.seribu}</label> , className: "text-right"},
            {content: <label>{x.lima_ratus}</label> , className: "text-right"},
            {content: <label>{x.seratus}</label> , className: "text-right"},
            {content: <label>{x.limapuluh}</label> , className: "text-right"},
            {content: <label>{"Rp. " + x.sisa}</label> , className: "text-right"},
            {content: x.total ? "" : <img className="icon-24x24" style={{"margin": "auto"}}
             src={deleteicon} alt=""
              onClick={() => {this.props.deleteRow(index) }} />, className: "text-center pr--25"
            }]
        }
        );

        let menu = <Menu className="wd--150" onClick={(item) => { this.props.addRow(item.key) }}>
            <Menu.Item key="5">{"add 5 row"}</Menu.Item>
            <Menu.Item key="10">{"add 10 row"}</Menu.Item>
            <Menu.Item key="15">{"add 15 row"}</Menu.Item>
        </Menu>

        return <div className="detil-tbl">
            <DynamicTable headers={header} rows={TableRow} />
            <div className="add-row">
                <Dropdown.Button
                    disabled={this.props.disabled}
                    className="btn-add-row"
                    onClick={() => this.props.addRow(1)}
                    overlay={menu}
                    trigger={['click']}>
                    {"Add more rows"}&nbsp;
                </Dropdown.Button>
            </div>
        </div>
    }
}

DetailTable.defaultProps = {
    data: [],
    dataAmount: [],
    error: [],
    onBlurListItem: () => { },
}
// DetailTable.propTypes = {
//     // data: PropTypes.array,
//     addRow: PropTypes.func,
//     onChange: PropTypes.func,
//     error: PropTypes.array,
//     dataAmount: PropTypes.array,
//     deleteRow: PropTypes.func,

// }

export default connect((store) => store)(DetailTable);

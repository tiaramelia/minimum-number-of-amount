import Types from '../actions/types';
import amountModel from '../model/amount';

export default function reducer (state = {
    model: JSON.parse(JSON.stringify(amountModel)),
    listFraction:[
        100000,
        50000,
        20000,
        10000,
        5000,
        1000,
        500,
        100,
        50
    ],
    error: null
}, action) {
    switch (action.type) {
        case Types.TYPE_REQUEST_AMOUNT.FETCH_DATA: {
            state.model = action.payload  
            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_AMOUNT.ADD_ROW : {
            let model = Object.assign({}, state.model);
            model.data = model.data.concat(action.payload);
            return Object.assign({}, state, {model: Object.assign(state.model, model)}, {loading: false, error: null});
        }
        case Types.TYPE_REQUEST_AMOUNT.SET_DETAIL_ROW: {
            let new_state = Object.assign({}, state)
            new_state.model = Object.assign({}, new_state.model)
            new_state.model.data = [{
                action:"ADD",
                amount: "",
                seratus_ribu: 0,
                limapuluh_ribu: 0,
                duapuluh_ribu: 0,
                sepuluh_ribu: 0,
                lima_ribu: 0,
                seribu: 0,
                lima_ratus: 0,
                seratus: 0,
                limapuluh: 0
            }];
            return new_state
        }
        case Types.TYPE_REQUEST_AMOUNT.DELETE_ROW: {
            let new_state = Object.assign({}, state)
            new_state.model = Object.assign({}, new_state.model)
            new_state.model.data.splice(action.payload, 1)
            return new_state
        }
        case Types.TYPE_REQUEST_AMOUNT.CHANGE_VALUE: {
            let model = Object.assign({}, state.model);
            model.data[action.payload.index][action.payload.key] = action.payload.value;
    
            return Object.assign({}, state, {model: Object.assign(state.model, model)}, {loading: false, error: null});
        }
        case Types.TYPE_REQUEST_AMOUNT.REJECTED_AMOUNT: {
            state.error = action.payload
            return Object.assign({}, state)
        }
        default:
            return state
    }
}
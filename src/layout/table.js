'use strict';

import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Table extends Component {
    render () {
        return <table id={this.props.id} className={this.props.className} style={this.props.style}>{this.props.children}</table>;
    }
}

Table.propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
}

Table.defaultProps = {
    style: {},
    className: ""
}

class TableHead extends Component {
    render () {
        return <thead className={this.props.className}>{this.props.children}</thead>;
    }
}

TableHead.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
}

class TableBody extends Component {
    render () {
        let rows = this.props.children || this.props.rows.map((row, index) => {
            let columns = row.columns || row;

            return <TableRow key={index} columns={columns} className={this.props.rowClassName} />
        });
        let heading = this.props.className == "tbody-dynamic" ? {height:this.props.heightBody} : {}
        return <tbody style={heading} className={this.props.className}>{rows}</tbody>;
    }
}

TableBody.defaultProps = {
    rows: [],
    heightBody:330
}

TableBody.propTypes = {
    className: PropTypes.string,
    rowClassName: PropTypes.string,
    rows: PropTypes.array,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
}

class TableRow extends Component {
    render () {
        let columns = this.props.children || this.props.columns.map((column, index) => {
            if (typeof column === 'string') {
                if (this.props.rowHeading) {
                    return <TH key={index} content={column} />
                }

                return <TD key={index} content={column} />
            }

            if (column.heading || this.props.rowHeading) {
                return <TH key={index} className={column.className} content={column.content} colSpan={column.colSpan} />
            }

            return <TD key={index} className={column.className} content={column.content} colSpan={column.colSpan} />
        });

        return (
            <tr className={this.props.className}>{columns}</tr>
        );
    }
}

TableRow.propTypes = {
    className: PropTypes.string,
    rowHeading: PropTypes.bool,
    columns: PropTypes.array,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
}

class TH extends Component {
    render () {
        return <th className={this.props.className} colSpan={this.props.colSpan}>{this.props.children || this.props.content}</th>;
    }
}

TH.propTypes = {
    className: PropTypes.string,
    colSpan: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    content: PropTypes.any,
    children: PropTypes.any
}

class TD extends Component {
    render () {
        return <td className={this.props.className} colSpan={this.props.colSpan}>{this.props.children || this.props.content}</td>;
    }
}

TD.propTypes = {
    className: PropTypes.string,
    colSpan: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    content: PropTypes.any,
    children: PropTypes.any
}

class DynamicTable extends Component {
    constructor (props) {
        super(props);

        this.state = {
            rows: props.rows
        }
        this.ajax = false
    }

    componentWillReceiveProps (props) {
        this.setState({rows: props.rows});
    }

    render () {
        let rows = [];
        let rowClassName = this.props.rowClassName;
        let bodyClassname = this.props.bodyClassname;

        if (this.state.rows.length > 0) {
            rows = this.state.rows;
        } else {
            if (this.props.emptyRowClassName) {
                rowClassName = this.props.emptyRowClassName;

            }
            bodyClassname = ""

            if (typeof this.props.emptyRow === 'string' && this.props.ajax) {
                rows = [{columns: [{colSpan: this.props.headers.length, content: this.props.emptyRow, className: 'text-center'}]}];
            }

        }
        return (
            <div className={this.props.classDivTable}>
                    <Table id={this.props.id} className={"table content--table table-hover " + this.props.className} style={this.props.style}>
                        <TableHead className={"dynamic-header-table " + this.props.headClassname}>
                            <TableRow columns={this.props.headers} rowHeading={true} />
                        </TableHead>
                        <TableBody rows={rows} rowClassName={rowClassName} className={bodyClassname} heightBody={this.props.heightBody}/>
                    </Table>
            </div>
        );
    }
}

DynamicTable.propTypes = {
    id: PropTypes.string,
    headers: PropTypes.array,
    rowClassName: PropTypes.string,
    emptyRowClassName: PropTypes.string,
    rows: PropTypes.array,
    headClassname: PropTypes.string,
    bodyClassname: PropTypes.string,
    emptyRow: PropTypes.any,
    classDivTable: PropTypes.string,
}

DynamicTable.defaultProps = {
    headers: [],
    rows: [],
    classDivTable:"",
    headClassname: "",
    bodyClassname: "",
    emptyRow: 'No result.',
    className:"",
    style:{},
    ajax:true
}

export {Table, TableHead, TableBody, TableRow, TH, TD, DynamicTable};
